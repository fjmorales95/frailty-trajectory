# LEEME



El archivo `merged_files.csv` contiene las secciones A, AA, C, D, E, H, I  de las olas 2001-2015 unidas por individuo junto con el master_follow_up_file_2015.dta para obtener las características e identificadores de los individuos. Para ver el significado de cada una de las variables, por favor referirse a la [documentación](http://www.enasem.org/DocumentationQuestionnaire_Esp.aspx) de la enasem. 

# Conteos

El archivo cuenta con 23,674 registros en 2,498 columnas.