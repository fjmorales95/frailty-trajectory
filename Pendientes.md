Se calculó el indice de tres maneras:

1. Casos completos 

2. Reemplazar NA por 0

   Es estadísticamente diferente al primer caso reduciendo el valor de la media, sd, mediana

3. Omitir NA para cada individuo 
   
   1. reduce el denominador





# Pendientes

+ ~~Obtener el indice con todas las variables y determinar cuáles con las variables que se pueden "desechar" sin cambiar el índice.~~
+ Comprobar la variabilidad del índice a diferente número de preguntas.
  + ~~alfa de cronbach~~
+ Componentes principales
+ ¿Correlación parejas?



**LA IDEA ES DESCRIBIR**





+ ~~Hacer las trayectorias usando las 52 variables.~~
  + ¿Por qué el índice es mayor en 2001 en comparación con 2003?
  + Agregar fallecido_15
+ Diferencias del indice (2012-2003)
  + Nota: .02 incremento anual esperado
+ Pendiente de la trayectoria
  + vs mortalidad
  + vs adl_clsf
  + vs mortalidad & adl_clsf
  + 2001:2003 vs 2012:2015
+ Edad
  + biológica: Minitsky lo pone en un articulo
+ Cluster
  + Edad